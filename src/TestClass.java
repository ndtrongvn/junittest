
public class TestClass {
	protected double sumPrice;
	protected int jupiter;
	protected int sirius;
	
	public TestClass() {
		
	}

//	public TestClass(double sumPrice) {
//		this.sumPrice = sumPrice;
//	}
	
	public TestClass(int sirius, int jupiter) {
		this.jupiter = jupiter;
		this.sirius = sirius;
		setSumPrice();
	}

	public double getSumPrice() {
		return sumPrice;
	}

	public void setSumPrice() {
		
		if (this.sirius < 0 || this.sirius > 100 || this.jupiter < 0 || this.jupiter > 50) {
			throw new IllegalArgumentException("Over limitation");
		}else {
			this.sumPrice = this.jupiter * 20 + this.sirius * 15;
		}
	}
}
//}
//class Jupiter{
//	protected double quantity;
//	
//	public Jupiter() {
//		
//	}
//	public Jupiter(double quantity) {
//		this.quantity = quantity;
//	}
//	public double getQuantity() {
//		return quantity;
//	}
//	public void getQuantity(double quantity) {
//		this.quantity = quantity;
//	}
//	
//	
//}
//class Sirius{
//	protected double quantity;
//	
//	public Sirius(double quantity) {
//		this.quantity = quantity;
//	}
//
//	public double getQuantity() {
//		return quantity;
//	}
//
//	public void setQuantity(double quantity) {
//		this.quantity = quantity;
//	}
//	
//}
